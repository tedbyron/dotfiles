<div align="center">
  <h1><code>dotfiles</code></h1>

  <p>
    <strong><a href="https://github.com/tedbyron">@tedbyron</a>'s dotfiles.</strong>
  </p>

  <img src="./.config/screen.png" alt="screenshot of terminal" />
</div>
